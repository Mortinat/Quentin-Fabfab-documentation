+++
title = "Home"
+++

Welcome to my Digital Fabrication Documentation home page.



{{< figure src="img/portrait.svg" title="Arsene Lupin" caption="Test caption to explain what's in the picture." height="300px" >}}

## Home page layout

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet magna varius tellus molestie pretium. Cras nec mattis libero. Nulla quis sem vel nunc dictum vestibulum. Nulla facilisi. Curabitur sed neque at tellus blandit vehicula. Vestibulum porttitor tortor non tortor porttitor, eu porttitor risus dignissim. Cras consequat, eros nec rutrum ornare, mauris odio viverra lectus, quis mattis metus sem id erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut bibendum pulvinar libero in euismod. Pellentesque lacus augue, pretium sit amet elit non, auctor fermentum lacus. Suspendisse ut erat auctor, efficitur tortor quis, sollicitudin libero. Aliquam erat volutpat. Ut tempor eget sem vitae posuere. Curabitur rhoncus nisi sed enim ultrices sagittis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lobortis tempor risus, in facilisis mi.

Cras lobortis, arcu et scelerisque euismod, arcu ipsum semper arcu, ut tincidunt justo magna a urna. Donec commodo tortor gravida, dignissim lacus ut, luctus velit. Donec vitae imperdiet diam, sed convallis tortor. Morbi feugiat purus ornare diam tempor, ac commodo arcu sagittis. In varius justo non dignissim facilisis. Sed non pretium eros. Vestibulum egestas, dolor sit amet sagittis egestas, neque nisi sodales erat, quis rutrum orci purus id diam. Aliquam erat volutpat. Aenean sit amet bibendum risus. 

## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading


## Horizontal Rules

___

---

***

## Emphasis

**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~


## Blockquotes


> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.


## Lists

Unordered

+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!

Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa


1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`

Start numbering with offset:

57. foo
1. bar


## Code

Inline `code`

Block code "fences"

```
Sample text here...
```

Syntax highlighting

``` js
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
```

## Tables

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

Right aligned columns

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |


## Links

[link text](http://dev.nodeca.com)

[link with title](http://nodeca.github.io/pica/demo/ "title text!")

Autoconverted link https://github.com/nodeca/pica (enable linkify to see)


## Images

![Minion](https://octodex.github.com/images/minion.png)
![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")

Like links, Images also have a footnote style syntax

![Alt text][id]

With a reference later in the document defining the URL location:

[id]: https://octodex.github.com/images/dojocat.jpg  "The Dojocat"
